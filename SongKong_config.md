# Main interface  
  ![Main.png](./img/Main.png)  
  
# Fix songs menu  
### Basic  
  ![Basic.png](./img/Basic.png)  

### Match  
  ![Match.png](./img/Match.png)
  
### Album Artwork  
  ![Album_Artwork.png](./img/Album_Artwork.png)
  
### Other Artwork  
  ![Other_Artwork.png](./img/Other_Artwork.png)
  
### Genres  
  ![Genres.png](./img/Genres.png)
  
### Format  
  ![Format.png](./img/Format.png)
  
### Classical  
  ![Classical.png](./img/Classical.png)
  
### File Naming  
  ![File_Naming.png](./img/File_Naming.png)
  
### Save  
  ![Save.png](./img/Save.png)
  

